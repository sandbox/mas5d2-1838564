<?php
/**
 * @file
 * Contains the CustomVarsVar and CustomVarsReal classes.
 */

define('CUSTOM_VARS_PREFIX', 'customvars__');

class CustomVarsVar {
  protected $id_;
  protected $display_name_;
  protected $value_;
  protected $hint_;
  protected $storage_type_ = 'textfield';
  protected $new_ = FALSE;

  protected function makeId($id) {
    $id = str_replace(' ', '_', strtolower($id));
    return $id;
  }

  protected function makeSystemVarName($id) {
    return CUSTOM_VARS_PREFIX . $id;
  }

  protected function loadDefaults($id) {
    $values = array(
      'id' => $id,
      'display_name' => $id,
      'value' => '[' . $id . ']',
      'hint' => '',
      'storage_type' => 'textfield',
    );

    $this->loadValues($values);
    $this->new_ = TRUE;
  }

  protected function loadValues($values) {
    foreach ($values as $k => $v) {
      $this->{$k . '_'} = $v;
    }
  }

  public function __construct($id) {
    $id = $this->makeId($id);
    $var_id = $this->makeSystemVarName($id);
    $system_var = variable_get($var_id, NULL);

    if ($system_var == NULL) {
      $this->loadDefaults($id);
    }
    else {
      $this->loadValues(json_decode($system_var));
    }
  }

  public function isNew() {
    return $this->new_;
  }

  public function getValue() {
    return $this->value_;
  }

  public function setValue($val) {
    $this->value_ = $val;
  }

  public function getDisplayName() {
    return $this->display_name_;
  }

  public function setDisplayName($val) {
    $this->display_name_ = $val;
  }

  public function getHint() {
    if ($this->hint_ == '') {
      return ' ';
    }
    return $this->hint_;
  }

  public function setHint($val) {
    $this->hint_ = $val;
  }

  public function getStorageType() {
    return $this->storage_type_;
  }

  public function setStorageType($var) {
    $this->storage_type_ = $var;
  }

  public function save() {
    $var_id = $this->makeSystemVarName($this->id_);
    $values = array(
      'id' => $this->id_,
      'display_name' => $this->display_name_,
      'value' => $this->value_,
      'hint' => $this->hint_,
      'storage_type' => $this->storage_type_,
    );

    variable_set($var_id, json_encode($values));
  }

  public function delete() {
    $var_id = $this->makeSystemVarName($this->id_);
    variable_del($var_id);
  }
}

class CustomVarsReal {
  public function get($id, $args) {
    $obj = new CustomVarsVar($id);

    if ($obj->isNew()) {
      $obj->save();
    }

    $val = $obj->getValue();
    if (is_array($args)) {
      $val = t($val, $args);
    }

    return t($val);
  }

  public function getFull($id) {
    $obj = new CustomVarsVar($id);

    return $obj;
  }

  public function getList() {
    $query = db_select('variable', 'v')
      ->fields('v', array('name'))
      ->condition('v.name', CUSTOM_VARS_PREFIX . '%', 'LIKE')
      ->execute();

    // TODO: Clean this up so we don't have to filter through it.
    $response = array();
    foreach ($query->fetchAll() as $v) {
      $name = $v->name;
      $name = substr($name, (strpos($name, '__') + 2));
      $response[] = $name;
    }

    return $response;
  }
}
