README.txt
==========

This module is used to allow site builders and developers an easy way to drop
placeholder text into code so it can be updated by content entry and copywriters
at a later date without having to modify code.

Frequently in the process of building a site we'll add Lorem Ipsum text to a
view header or a a custom form, only to have to go replace that text later on.
Or worse, we'll forget to replace it, it gets lost in UAT/QA and three days
after a production push we need another emergency production push to remove
some errant Lorem Ipsum.

This module attempts to solve that issue by allowing the developer to drop
in a system variable at any point in their PHP code, and instantly creating
a coresponding entry in the 'Custom Variables' admin page that can be edited
from inside an administrative UI. The variable doesn't need to be added first,
as the first time the variable is ever accessed (displayed on a page) an entry
is created and can then be updated. Not only does this offload the copy edits
to content entry folks, but also provides a convienenty one stop page to see
all of the Lorem Ipsum or placeholder text you've written in one place.


SETUP
=====

To setup, turn the module on and start using it in your code as per the
HOWTO USE sections.

You'll want to setup the permissions as well. The module uses two permissions...

  - Manage custom variable values: Manage the text values of each variable.
    Give this permission to content administators and copy writers.

  - Manage custom variable details: Update the details of a variable such as
    display name, hint text and storage type. Reserve this permission for
    developers and site builders.


HOWTO USE -- DEVELOPERS/SITEBUILDERS
====================================

------
Basics
------
Anywhere you need a custom variable use the following snippet of code ...

<?php print CustomVars::Get('MY_CUSTOM_KEY'); ?>

Where MY_CUSTOM_KEY is any string key, preferably with no spaces. By default
the displayed bit of text when printed will be [MY_CUSTOM_KEY] and you're free
to go update that value to something nicer at admin/structure/custom_variables.

Remember, the code has to run at least once before the variable will be
available for updating/modification on the administrative page.

Anyone with the 'Manage custom variable details' can also modify the details of
a variable, changing the display name from the default key, changing the 'Hint'
text that is displayed for each variable to copy writers, and updating the
storage type from textfield to textarea to provide a larger area to store text.

----------
A bit more
----------
If you need to use custom code inside your custom strings, such as the name of
the logged in user, you can supply a second argument to CustomVars::Get() as
an array in the form of 'key' => 'value' to do simple string replacement via
t().

For example use ...

<?php print CustomVars::Get('MY_CUSTOM_KEY', array('!time' => REQUEST_TIME)); ?>

Update the value of MY_CUSTOM_KEY at admin/structure/custom_variables to
something along the lines of ...

'The current time is !time.' and the resulting output will be

> The current time is 1363820660.


HOWTO USE -- CONTENT EDITORS/COPYWRITERS
========================================

Content editors and copywriters can update the values of custom variables at
admin/structure/content_variables.

All custom variables are listed on this page in drop-down fieldsets. Simply
update the value and click 'Save'. It's all done ajax-y so no page reloading
needs to be done (except to get new variables if there are new variables).

Keep in mind that we don't do any filtering on the text in these fields, so you
can use HTML here. This is pretty important to note as it can be a security
issue to give permission to folks that aren't trusted to be cafeful in the
values they add here.
