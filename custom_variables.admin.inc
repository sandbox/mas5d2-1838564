<?php
/**
 * @file
 * The admin form and ajax callbacks for the custom variables module.
 */

/**
 * The form located at admin/structure/custom_variables.
 *
 * For setting hte values and altering the details of custom variables.
 */
function custom_variables_admin_form($form, &$form_state) {
  $manage_details = user_access('custom variables manage details');
  $add_variable = user_access('custom variables add');
  $delete_variable = user_access('custom variables delete');

  if ($add_variable) {
    $form['manage'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage'),
    );

    $form['manage']['key'] = array(
      '#type' => 'textfield',
      '#title' => 'Key',
    );

    $form['manage']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Create New Variable',
    );
  }

  foreach (CustomVars::GetInstance()->getList() as $id) {
    $var = CustomVars::GetFull($id);
    $title = $var->getDisplayName();
    $type = $var->getStorageType();
    if ($title != $id && $title != '[' . $id . ']') {
      $title .= ' [' . $id . ']';
    }
    $form[$id] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($title),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form[$id][$id . '__value_textarea'] = array(
      '#type' => 'textarea',
      '#value' => $var->getValue(),
      '#attributes' => array('style' => 'display:none'),
      '#resizable' => FALSE,
    );

    $form[$id][$id . '__value_textfield'] = array(
      '#type' => 'textfield',
      '#value' => $var->getValue(),
      '#description' => filter_xss(trim($var->getHint())),
      '#attributes' => array('style' => 'display:none'),
    );

    switch ($type) {
      case 'textarea':
        $form[$id][$id . '__value_textarea']['#attributes'] = array();
        break;

      default:
        $form[$id][$id . '__value_textfield']['#attributes'] = array();
    }

    if ($manage_details) {
      $form[$id][$id . '__details'] = array(
        '#type' => 'fieldset',
        '#collapsible' => FALSE,
        '#attributes' => array('style' => 'display: none'),
      );
      $form[$id][$id . '__details'][$id . '__display_name'] = array(
        '#type' => 'textfield',
        '#title' => 'Display Name',
        '#value' => $var->getDisplayName(),
      );
      $form[$id][$id . '__details'][$id . '__hint'] = array(
        '#type' => 'textfield',
        '#title' => 'Hint',
        '#value' => trim($var->getHint()),
      );
      $form[$id][$id . '__details'][$id . '__input_type'] = array(
        '#type' => 'select',
        '#title' => 'Input Type',
        '#options' => array(
          'textfield' => t('Text Field'),
          'textarea' => t('Text Area'),
        ),
        '#value' => $type,
      );
      $form[$id][$id . '__details'][$id . '____save_details'] = array(
        '#type' => 'button',
        '#value' => 'Save Details',
        '#name' => $id . '____save_details',
        '#ajax' => array(
          'callback' => 'custom_variables_edit_details_ajax_callback',
        ),
      );
    }
    $form[$id][$id . '____submit'] = array(
      '#type' => 'button',
      '#value' => 'Save Value',
      '#title' => "test",
      '#name' => $id,
      '#ajax' => array(
        'callback' => 'custom_variables_edit_value_ajax_callback',
      ),
    );

    if ($manage_details) {
      $form[$id][$id . '____edit'] = array(
        '#type' => 'button',
        '#value' => 'Edit Details',
        '#name' => $id . '____edit',
        '#ajax' => array(
          'callback' => 'custom_variables_edit_details_ajax_callback',
        ),
      );
    }

    if ($delete_variable) {
      $form[$id][$id . '____delete'] = array(
        '#type' => 'button',
        '#value' => 'Delete Variable',
        '#name' => $id . '____delete',
        '#ajax' => array(
          'callback' => 'custom_variables_delete_ajax_callback',
        ),
      );
    }

    $code = '<code>&lt?php print CustomVars::Get(\'' . $id . '\'); ?></code>';
    $form[$id]['help'] = array(
      '#markup' => '<div>' . t('Use !code to print this variable.',
                     array('!code' => $code)) . '</div>',
    );

  }

  return $form;
}

/**
 * Validate new variable creation.
 */
function custom_variables_admin_form_validate($form, &$form_state) {
  if ($form_state['input']['op'] != 'Create New Variable') {
    return;
  }

  $add_variable = user_access('custom variables add');
  if (!$add_variable) {
    form_set_error('manage', t('Permission denied.'));
    return;
  }

  $key = $form_state['values']['key'];
  $key = strtolower(trim($key));
  $matches = array();
  $result = preg_match('/[A-Za-z0-9\_\.]+/', $key, $matches);
  if ($result && $matches[0] == $key) {
    // Good!
    $var = CustomVars::GetFull($key);
    if (!$var->isNew()) {
      $err = t('Custom variable keys must be unique. That key is already being used..');
      form_set_error('key', $err);
    }
  }
  else {
    $err = t('Custom variable keys must contain only letters, numbers, underscores (_) and periods (.). Spaces are not allowed.');
    form_set_error('key', $err);
  }

}

/**
 * Handle new variable submission and creation.
 */
function custom_variables_admin_form_submit($form, &$form_state) {
  if ($form_state['input']['op'] != 'Create New Variable') {
    return;
  }
  $key = $form_state['values']['key'];
  $key = strtolower(trim($key));
  $var = CustomVars::Get($key);

  drupal_set_message(t("Custom variable with the key '!key' has been created.", array('!key' => $key)));
}

/**
 * Callback for deleting a variable.
 */
function custom_variables_delete_ajax_callback($form, &$form_state) {
  if (!user_access('custom variables delete')) {
    // This should never happen. Silently do nothing.
    return;
  }

  $trigger = explode('____', $form_state['input']['_triggering_element_name']);
  $id = $trigger[0];

  $var = CustomVars::GetFull($id);
  $name = $var->getDisplayName();

  $var->delete();

  $selector = str_replace('_', '-', $id);

  $success = $selector . '-success-' . REQUEST_TIME;
  $msg = t('This variable has been deleted.');
  $msg = '<div id="' . $success . '" class="status messages error"><pre>' .
    $msg . '</pre></div>';

  $commands = array();

  $commands[]
    = ajax_command_remove('#edit-' . $selector . ' .fieldset-wrapper div');
  $commands[]
    = ajax_command_remove('#edit-' . $selector . ' .fieldset-wrapper input');
  $commands[]
    = ajax_command_remove('#edit-' . $selector . ' .fieldset-wrapper fieldset');

  $commands[]
    = ajax_command_append('#edit-' . $selector . ' .fieldset-wrapper', $msg);

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Callback for handling the saving of variable details.
 */
function custom_variables_edit_details_ajax_callback($form, &$form_state) {
  if (!user_access('custom variables manage details')) {
    // This should never happen. Silently do nothing.
    return;
  }

  $trigger = explode('____', $form_state['input']['_triggering_element_name']);

  $id = $trigger[0];
  $replacements = array(
    '__________' => '-',
    '_________' => '-',
    '________' => '-',
    '_______' => '-',
    '______' => '-',
    '_____' => '-',
    '____' => '-',
    '___' => '-',
    '__' => '-',
    '_' => '-',
  );

  $selector = str_replace(array_keys($replacements), array_values($replacements), $id);
  $commands = array();

  $display_block = array('display' => 'block');
  $display_none = array('display' => 'none');

  if ($trigger[1] == 'edit') {
    $commands[]
      = ajax_command_css('#edit-' . $selector . '-details', $display_block);
    $commands[]
      = ajax_command_css('#edit-' . $selector . '-submit', $display_none);
    $commands[]
      = ajax_command_css('#edit-' . $selector . '-edit', $display_none);
  }
  elseif ('save_details' == $trigger[1]) {
    $display_name = $form_state['input'][$id . '__display_name'];
    $hint = $form_state['input'][$id . '__hint'];
    $type = $form_state['input'][$id . '__input_type'];

    $var = CustomVars::GetFull($id);

    $var->setDisplayName($display_name);
    $var->setHint($hint);
    $var->setStorageType($type);
    $var->save();

    $replace_name = $display_name;

    if ($display_name != $id && $display_name != '[' . $id . ']') {
      $replace_name .= ' [' . $id . ']';
    }
    switch ($type) {
      case 'textarea':
        $commands[]
          = ajax_command_css('#edit-' . $selector . '-value-textarea',
                             $display_block);
        $commands[]
          = ajax_command_css('#edit-' . $selector . '-value-textfield',
                             $display_none);
        break;

      default:
        $commands[]
          = ajax_command_css('#edit-' . $selector . '-value-textarea',
                             $display_none);
        $commands[]
          = ajax_command_css('#edit-' . $selector . '-value-textfield',
                             $display_block);
        break;

    }

    $commands[]
      = ajax_command_html('#edit-' . $selector . ' .fieldset-title',
                          '<span>' . $replace_name . '</span>');
    $commands[]
      = ajax_command_html('#edit-' . $selector . ' .form-item-' . $selector .
                          '--value-textarea .description',
                          '<span>' . $hint . '</span>');
    $commands[]
      = ajax_command_html('#edit-' . $selector . ' .form-item-' . $selector .
                          '--value-textfield .description',
                          '<span>' . $hint . '</span>');

    $commands[]
      = ajax_command_css('#edit-' . $selector . '-details',
                         $display_none);
    $commands[]
      = ajax_command_css('#edit-' . $selector . '-submit',
                         array('display' => 'inline-block'));
    $commands[]
      = ajax_command_css('#edit-' . $selector . '-edit',
                         array('display' => 'inline-block'));

    $success = $selector . '-success-' . REQUEST_TIME;

    $msg = t('Saved details for !name.',
             array('!name' => $var->getDisplayName()));
    $msg = '<div id="' . $success . '" class="status messages"><pre>' .
      $msg . '</pre></div>';

    $commands[]
      = ajax_command_before('.form-item-' . $selector . '--value-textarea',
                            $msg);
    $commands[]
      = ajax_command_invoke('#' . $success, 'fadeOut', array(5000));

  }

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Callback for handling the saving of variable values.
 */
function custom_variables_edit_value_ajax_callback($form, $form_state) {
  if (!user_access('custom variables manage values')) {
    // This should never happen. Silently do nothing.
    return;
  }

  $id = $form_state['input']['_triggering_element_name'];
  $value = $form_state['input'][$id . '__value_textfield'];
  $type = $form_state['input'][$id . '__input_type'];
  switch ($type) {
    case 'textarea':
      $value = $form_state['input'][$id . '__value_textarea'];
  }

  $var = CustomVars::GetFull($id);
  $var->setValue($value);
  $var->save();

  $selector = str_replace('_', '-', $id);

  $success = $selector . '-success-' . REQUEST_TIME;
  $msg = t('Success! Updated value for !name.',
           array('!name' => $var->getDisplayName()));
  $msg = '<div id="' . $success . '" class="status messages"><pre>' .
    $msg . '</pre></div>';

  $commands = array();
  $commands[]
    = ajax_command_before('.form-item-' . $selector . '--value-textarea',
                          $msg);
  $commands[]
    = ajax_command_invoke('#' . $success, 'fadeOut', array(5000));

  return array('#type' => 'ajax', '#commands' => $commands);
}
